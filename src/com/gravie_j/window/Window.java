package com.gravie_j.window;

import com.gravie_j.button.SimpleButton;
import com.gravie_j.evalExpr.EvalExpr;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Simple Count
 *
 * Created by gravie_j on 03/11/2014.
 */
public class Window extends JFrame implements ActionListener {

    private ArrayList<SimpleButton> mButtonList;
    private JPanel mTextPanel;
    private JLabel mTextField;

    private BoxLayout mMainLayout;
    private GridLayout mButtonLayout;

    private JPanel mButtonPanel;

    private String[] mButtons = {"AC", "+/-", "%", "/", "7", "8", "9", "*", "4", "5", "6", "-", "1", "2", "3", "+", "0", " ", ".", "="};

    private String mExpr;

    @Override
    public void actionPerformed(ActionEvent event) {
        SimpleButton button = (SimpleButton) event.getSource();

        if (button.isNumber())
            mExpr += button.getText();
        else if (button.isOperator())
            mExpr += " " + button.getText() + " ";
        else {
            if (button.getText().equals("=")) {
                EvalExpr toEval = new EvalExpr();
                mExpr = toEval.eval(mExpr);
//                ScriptEngineManager manager = new ScriptEngineManager();
//                ScriptEngine engine = manager.getEngineByName("js");
//                Object result = null;
//                try {
//                    result = engine.eval(mExpr);
//                    mExpr = String.valueOf(result);
//                } catch (ScriptException e) {
//                    mExpr = "Error ";
//                }
            }
        }

        mTextField.setText(mExpr);
    }

    public Window() {
        super();

        mButtonList = new ArrayList<SimpleButton>();
        mTextPanel = new JPanel();
        mTextField = new JLabel("", SwingConstants.RIGHT);

        mMainLayout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
        mButtonLayout = new GridLayout(5, 4);

        mButtonPanel = new JPanel();

        mExpr = "";

        setTitle("Simple Count");
        setSize(232, 322);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        setLayout(mMainLayout);
        add(mTextPanel);
        add(mButtonPanel);

        mButtonPanel.setLayout(mButtonLayout);

        mTextField.setFont(new Font("Helvetica", Font.PLAIN, 30));
        mTextField.setForeground(Color.WHITE);
        mTextField.setBorder(null);
        mTextField.setPreferredSize(new Dimension(300, 70));

        mTextPanel.setMaximumSize(new Dimension(300, 10));
        mTextPanel.setLayout(new BorderLayout());
        mTextPanel.add(mTextField);
        mTextPanel.setBackground(new Color(55, 52, 65));

        for (int i = 0; i < 20; i++) {
            SimpleButton button = new SimpleButton(mButtons[i]);

            mButtonList.add(button);
            mButtonPanel.add(button);

            button.addActionListener(this);
        }
    }
}
