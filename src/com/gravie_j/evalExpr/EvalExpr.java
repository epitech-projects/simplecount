package com.gravie_j.evalExpr;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by gravie_j on 10/11/2014.
 */
public class EvalExpr {
    private String[] mOperators = {"/*%", "-+"};
    private List<String> mParsedExpr;

    private Map<String, String> mFunctionMap= new HashMap<String, String>();

    public EvalExpr() {
        mFunctionMap.put("+", "add");
        mFunctionMap.put("-", "subtract");
        mFunctionMap.put("/", "divide");
        mFunctionMap.put("*", "multiply");
        mFunctionMap.put("%", "modulo");
    }

    public void add(int i) {
        Double a = Double.parseDouble(mParsedExpr.get(i - 1));
        Double b = Double.parseDouble(mParsedExpr.get(i + 1));
        Double result = 0.0;

        result = a + b;
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.add(i - 1, result.toString());
    }

    public void subtract(int i) {
        Double a = Double.parseDouble(mParsedExpr.get(i - 1));
        Double b = Double.parseDouble(mParsedExpr.get(i + 1));
        Double result = 0.0;

        result = a - b;
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.add(i - 1, result.toString());
    }

    public void divide(int i) {
        Double a = Double.parseDouble(mParsedExpr.get(i - 1));
        Double b = Double.parseDouble(mParsedExpr.get(i + 1));
        Double result = 0.0;

        result = a / b;
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.add(i - 1, result.toString());
    }

    public void multiply(int i) {
        Double a = Double.parseDouble(mParsedExpr.get(i - 1));
        Double b = Double.parseDouble(mParsedExpr.get(i + 1));
        Double result = 0.0;

        result = a * b;
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.add(i - 1, result.toString());
    }

    public void modulo(int i) {
        Double a = Double.parseDouble(mParsedExpr.get(i - 1));
        Double b = Double.parseDouble(mParsedExpr.get(i + 1));
        Double result = 0.0;

        result = a % b;
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.remove(i - 1);
        mParsedExpr.add(i - 1, result.toString());
    }

    private void parse(String expr) {
        mParsedExpr = new ArrayList<String>(Arrays.asList(expr.split(" ")));
    }

    public String eval(String expr) {
        parse(expr);

        for (int priority = 0; priority < 2; priority++) {
            for (int i = 0; i < mParsedExpr.size(); i++) {
                String current = mParsedExpr.get(i);

                if (mOperators[priority].contains(current) && current.length() > 0) {
                    try {
                        System.out.println("i: " + i);
                        System.out.println("expr: " + expr);
                        System.out.println("current length: " + current.length());
                        System.out.println("Current: " + current);
                        Method method = this.getClass().getMethod(mFunctionMap.get(current), int.class);
                        method.invoke(this, i);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    i = -1;
                }
            }
        }
        return (mParsedExpr.get(0).replaceAll("[.0]+$", ""));
    }
}
