package com.gravie_j.button;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import java.awt.*;

/**
 * Created by gravie_j on 06/11/2014.
 */
public class SimpleButton extends JButton {

    public boolean isFunction() {
        return mIsFunction;
    }

    public boolean isOperator() {
        return mIsOperator;
    }

    public boolean isNumber() {
        return mIsNumber;

    }

    private boolean mIsNumber = false;
    private boolean mIsOperator = false;
    private boolean mIsFunction = false;

    private String mText = "";

    public SimpleButton(String text) {
        super(text);

        mText = text;

        setBackground(new Color(214, 214, 214));
        setFont(new Font("Helvetica", Font.PLAIN, 15));
        setBorder(new MatteBorder(0, 0, 1, 1, new Color(142, 142, 142)));

        if (".0123456789".contains(text)) {
            mIsNumber = true;
            setBackground(new Color(224, 224, 224));
        }
        else if ("%/*-+".contains(text))
            mIsOperator = true;
        else
            mIsFunction = true;

        if (mIsOperator && !text.equals("%")) {
            setBorder(new MatteBorder(0, 0, 1, 0, new Color(142, 142, 142)));
            setBackground(new Color(245, 146, 62));
            setForeground(Color.WHITE);
        }
        else if ("0 .".contains(text))
            setBorder(new MatteBorder(0, 0, 0, 1, new Color(142, 142, 142)));

        if (text.equals("=")) {
            setBackground(new Color(245, 146, 62));
            setForeground(Color.WHITE);
            setBorder(null);
        }

        setOpaque(true);
    }
}